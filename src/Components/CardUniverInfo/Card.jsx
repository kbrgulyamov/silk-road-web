import React, { useEffect } from 'react'
import icon from '../CardUniverInfo/icons/choice 1.svg'
import btnIcon from '../CardUniverInfo/icons/down 1.svg'
import choice from '../CardUniverInfo/icons/choice 1.svg'
import support from '../CardUniverInfo/icons/support 1.svg'
import thought from '../CardUniverInfo/icons/thought 1.svg'
import Aos from "aos";
import "aos/dist/aos.css"




function CardsInfoUni(props) {
      useEffect(() => {
            Aos.init({ duration: 2700 })
      }, [])
      return (
            <div className='wrapper_cards'>
                  <div className='cards__info__university' data-aos="fade-left">
                        <div className='text_insaet'>
                              <h1 className='title_card'>Why you should choose<br /> our university?</h1>
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Asperiores dolores sed et. Tenetur quia eos. Autem tempore quibusdam vel necessitatibus optio ad corporis.</p>
                              <div className="btn_card">
                                    <button>Learn More</button>
                                    <img className='icon' src={btnIcon} alt="" />
                              </div>
                        </div>
                  </div>

                  <div className="Card_Flexibility" data-aos="fade-left">
                        <div className='top_contaent'>
                              <img className='icon_2' src={choice} alt="" />
                              <div className="title_down">
                                    <p>Flexibility</p>
                                    <hr className='hr' />
                              </div>
                        </div>
                        <div className="down_text">
                              <p style={{ textAlign: 'center' }}>Our university provides<br /> different interesting clubs,<br /> events and trips.</p>
                        </div>
                  </div>

                  <div className="Card_Flexibility" data-aos="fade-left">
                        <div className='top_contaent'>
                              <img className='icon_2' src={support} alt="" />
                              <div className="title_down">
                                    <p>Friendliness</p>
                                    <hr className='hr' />
                              </div>
                        </div>
                        <div className="down_text">
                              <p style={{ textAlign: 'center' }}>Meet friendly students,<br /> teachers and enjoy your<br /> studying.</p>
                        </div>
                  </div>

                  <div className="Card_Flexibility" data-aos="fade-left">
                        <div className='top_contaent'>
                              <img className='icon_2' src={thought} alt="" />
                              <div className="title_down">
                                    <p>Knowledge</p>
                                    <hr className='hr' />
                              </div>
                        </div>
                        <div className="down_text">
                              <p style={{ textAlign: 'center' }}>Boost your knowledge with<br /> us. Improve your skills in<br /> tourism sphere.</p>
                        </div>
                  </div>

                  <div className="Card_Flexibility" data-aos="fade-left">
                        <div className='top_contaent'>
                              <img className='icon_2' src={choice} alt="" />
                              <div className="title_down">
                                    <p>Flexibility</p>
                                    <hr className='hr' />
                              </div>
                        </div>
                        <div className="down_text">
                              <p style={{ textAlign: 'center' }}>Our university provides<br /> different interesting clubs,<br /> events and trips.</p>
                        </div>
                  </div>


            </div>
      )
}

export default CardsInfoUni