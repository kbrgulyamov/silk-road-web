import React, { useState } from 'react'
import ModalWindow from '../Modalwindow/modal'


const HeaderW = (props) => {
      const [modalActive, setModalActive] = useState(false)

      return (
            <header>
                  <div className='container_header'>
                        <div className="contaent_header">

                              <div className='logo'>
                                    <img src={props.logoImg} className='logo_header' alt="" />
                              </div>

                              <div className="link_pages">
                                    <a href="#">Home</a>
                                    <a href="#">News</a>
                                    <a href="/Clubs">Clubs</a>
                                    <a href="/About">Abouts Us</a>
                                    <a href="Contact">Contact Us</a>
                              </div>

                        </div>
                  </div>
                  <div className="right_btn">
                        <div className="btn_login_header">
                              <button className='login_btn' onClick={() => setModalActive(true)}>Log In</button>
                              <ModalWindow active={modalActive} setActive={setModalActive} />
                        </div>
                  </div>


            </header>

      )
}

export default HeaderW