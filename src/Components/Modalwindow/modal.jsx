import React from 'react'

const ModalWindow = ({ active, setActive }) => {
  return (
    <div className={active ? "modal active " : "modal"} onClick={() => setActive(false)}>
      <div className={active ? "modal_conatent active" : "modal_conatent"} onClick={e => e.stopPropagation()}>
        <div className="modal_contaent modal_two">

          <div className='title_rigister'>
            <h1>Регистрация</h1>
          </div>

          <form action="" className='form'>
            <input type="text" required placeholder='Почта' />
            <input type="text" required placeholder='Имя' />
            <input type="text" required placeholder='Фамилия' />
            <input type='password' required placeholder='Пароль' />
            <div className="btn_sign">
              <button className='Sign_up_btn'>Sign Up</button>
              <a href="https://accounts.google.com/o/oauth2/auth/oauthchooseaccount?access_type=offline&client_id=805818759045-aa9a2emskmnmeii44krng550d2fd44ln.apps.googleusercontent.com&redirect_uri=https%3A%2F%2Fgitlab.com%2Fusers%2Fauth%2Fgoogle_oauth2%2Fcallback&response_type=code&scope=email%20profile&state=d0d983b57343d9d6f5bee94069166f3787958e710335636b&flowName=GeneralOAuthFlow">
                <button className='Google_auth'>Finish Sign Up With Google</button>
              </a>
              <img className='google_auther' src="https://s3-alpha-sig.figma.com/img/e5b0/1e93/7c1e3abf151df6de8915f2aed8da2c8d?Expires=1654473600&Signature=cPGN-nKUMMKpgJEFK5ZcJjM2Pa9nXLJwYz6k~iwbKaKuo-JA6neFV9cF3C1CBpqPC~zgPK9Wv6kxeo5Fa4vfkzmcCx~Y0tUBLoxzoQg7oEmDYXoBiaeCQ4Duq5KAfceVKfEjOLxjeNt6Rpt6PCrYoRbmRYdMS9ckv53d57K05dhmcZLkz-MhBQfepKCxowf0qRZM94gSiUy9P~fBWDzKzuFygZA3ImcDtbWwn-0K-nSpV5K5f~Ifukomw29b9sPtwvmJevAzuOvsW8yQlDNKQYkS5VEAHjHWSvsYibwZgUr3qHwmn8aokAClU5MfgKCMRtGReZWRZ~676cCWRiYl1g__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA" alt="" />
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ModalWindow