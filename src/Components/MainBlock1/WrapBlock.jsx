import React, { useState, useEffect } from 'react'
import Aos from "aos";
import "aos/dist/aos.css"
import CardsInfoUni from '../CardUniverInfo/Card';


function WrapBlock(props) {

      useEffect(() => {
            Aos.init({ duration: 2700 })
      }, [])
      return (
            <div className="bg_wrapper">
                  <div className="container">
                        <div className='Info_Silk_Text' data-aos="fade-right">
                              <h1>Silk Road Students Union website<br /> is created with purpose to show<br /> university life through the eyes of<br /> students!</h1>
                              <p>Sign up to explore more about International<br /> University Of Tourism And Cultural Heritage</p>
                              <button className='login_btn_bg'>Login In</button>
                        </div>
                        <CardsInfoUni />
                  </div>
            </div>

      )
}

export default WrapBlock