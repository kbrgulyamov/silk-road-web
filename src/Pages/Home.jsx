import React from 'react'
import HeaderW from '../Components/Header/header'
import logo from '../log/silk.png'
import WrapBlock from '../Components/MainBlock1/WrapBlock'
import PauseOnHover from '../Components/Slieder/slider'

function Home() {
      return (
            <div className='container_page'>
                  <HeaderW logoImg={logo} />
                  <WrapBlock />
                  <div className="container">
                        <PauseOnHover />
                  </div>
            </div>
      )
}

export default Home